class CreateTactics < ActiveRecord::Migration[5.2]
  def change
    create_table :tactics do |t|
      t.string :name
      t.timestamps
    end
  end
end
