class AddTeamToPlayer < ActiveRecord::Migration[5.2]
  def up
    add_column :players, :team_id, :bigint
    add_index :players, :team_id
  end

  def down
    remove_index :players, :team_id
    remove_column :players, :team_id, :bigint
  end
end
