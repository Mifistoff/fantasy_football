class CreatePositionCounts < ActiveRecord::Migration[5.2]
  def change
    create_table :position_counts do |t|
      t.references :position, foreign_key: true
      t.integer :count
      t.references :tactic, foreign_key: true
      t.timestamps
    end
  end
end
