class ChangePositionToTacticInClub < ActiveRecord::Migration[5.2]
  def up
    remove_index :clubs, :position_id
    remove_column :clubs, :position_id, :bigint

    add_column :clubs, :tactic_id, :bigint
    add_index :clubs, :tactic_id
  end

  def down
    remove_index :clubs, :tactic_id
    remove_column :clubs, :tactic_id, :bigint

    add_column :clubs, :position_id, :bigint
    add_index :clubs, :position_id
  end
end
