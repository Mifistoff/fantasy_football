class RemoveTacticFromClub < ActiveRecord::Migration[5.2]
  def up
    remove_index :clubs, :tactic_id
    remove_column :clubs, :tactic_id, :bigint
  end

  def down
    add_column :clubs, :tactic_id, :bigint
    add_index :clubs, :tactic_id
  end
end
