class PlayersPositions < ActiveRecord::Migration[5.2]
  def change
    create_table :players_positions, id: false do |t|
      t.belongs_to :player, index: true
      t.belongs_to :position, index: true
    end
  end
end
