# POSITIONS

positions_name = %w[
  GoalKeeper Defender Midfielder Winger Centre\ forward
]

Position.create( positions_name.map { |name| { name: name } } )

# CLUBS

clubs_name =
  %w[Fiorentina Lazio Inter SPAL Parma Juventus Napoli Torino Genoa Roma]

Club.create( clubs_name.map { |name| { name: name } } )

# PLAYERS

players_name = %w[
  Lafont Strakosha Skhriniar Miranda Felipe Cionek Iacoponi Milenkovic
  De\ Sciglio Husai De\ Silvestri Lazovic Lulic Nzonzi Lucas\ Leiva Vecino
  Brozovic Paqueta Perisic Suso Iago\ Falque Insigne Martinez Belotti Zapata
]

position_id = ->(position_name) { Position.find_by(name: position_name).id }

players_positions_ids =
  Array.new(2, position_id.call(positions_name[0]) ) +
  Array.new(8, position_id.call(positions_name[1]) ) +
  Array.new(8, position_id.call(positions_name[2]) ) +
  Array.new(4, position_id.call(positions_name[3]) ) +
  Array.new(3, position_id.call(positions_name[4]) )

players = []

25.times do |i|
  players << {
    name: players_name[i],
    club: Club.all.sample,
    position_ids: players_positions_ids[i]
  }
end

Player.create(players)

# TACTICS

tactic1 = Tactic.create( name: '1 - 3 - 5 - 2' )
tactic2 = Tactic.create( name: '1 - 4 - 3 - 3' )
tactic3 = Tactic.create( name: '1 - 4 - 4 - 2' )

tactic1.position_counts.create([
  { position_id: position_id.call(positions_name[0]), count: 1 },
  { position_id: position_id.call(positions_name[1]), count: 3 },
  { position_id: position_id.call(positions_name[2]), count: 5 },
  { position_id: position_id.call(positions_name[4]), count: 2 }
])

tactic2.position_counts.create([
  { position_id: position_id.call(positions_name[0]), count: 1 },
  { position_id: position_id.call(positions_name[1]), count: 4 },
  { position_id: position_id.call(positions_name[2]), count: 3 },
  { position_id: position_id.call(positions_name[3]), count: 2 },
  { position_id: position_id.call(positions_name[4]), count: 1 }
])

tactic3.position_counts.create([
  { position_id: position_id.call(positions_name[0]), count: 1 },
  { position_id: position_id.call(positions_name[1]), count: 4 },
  { position_id: position_id.call(positions_name[2]), count: 4 },
  { position_id: position_id.call(positions_name[4]), count: 2 }
])
