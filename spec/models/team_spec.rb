require 'rails_helper'

RSpec.describe Team, type: :model do
  it { should belong_to(:tactic) }

  it { should have_many(:players) }

  it { should validate_presence_of(:name) }
  it { should validate_uniqueness_of(:name) }
end
