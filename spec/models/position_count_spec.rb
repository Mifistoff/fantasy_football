require 'rails_helper'

RSpec.describe PositionCount, type: :model do
  it { should belong_to(:position) }
  it { should belong_to(:tactic) }

  it { should validate_presence_of(:count) }
end
