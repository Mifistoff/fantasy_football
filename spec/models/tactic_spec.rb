require 'rails_helper'

RSpec.describe Tactic, type: :model do
  it { should have_many(:position_counts).dependent(:destroy) }
  it { should have_many(:teams) }

  it { should validate_presence_of(:name) }
  it { should validate_uniqueness_of(:name) }
end
