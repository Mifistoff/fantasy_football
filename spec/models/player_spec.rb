require 'rails_helper'

RSpec.describe Player, type: :model do
  it { should belong_to(:club) }
  it { should belong_to(:team) }
  it { should have_and_belong_to_many(:positions) }

  it { should validate_presence_of(:name) }
  it { should validate_uniqueness_of(:name) }
end
