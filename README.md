# Fantasy Football

WEB API

## Teams [/teams]

### All teams [GET /teams]

+ Request (application/json)

+ Response 200 (application/json)

    + Body

            [
                {
                    "id": 1,
                    "name": "team 1",
                    "tactic": "1 - 4 - 3 - 3",
                    "players": [
                        {
                            "id": 2,
                            "name": "Strakosha"
                        },
                        {
                            "id": 6,
                            "name": "Cionek"
                        },
                        {
                            "id": 7,
                            "name": "Iacoponi"
                        },
                        {
                            "id": 8,
                            "name": "Milenkovic"
                        },
                        {
                            "id": 15,
                            "name": "Lucas Leiva"
                        },
                        {
                            "id": 21,
                            "name": "Iago Falque"
                        },
                        {
                            "id": 22,
                            "name": "Insigne"
                        },
                        {
                            "id": 3,
                            "name": "Skhriniar"
                        },
                        {
                            "id": 11,
                            "name": "De Silvestri"
                        },
                        {
                            "id": 16,
                            "name": "Vecino"
                        },
                        {
                            "id": 25,
                            "name": "Zapata"
                        }
                    ]
                }
            ]

### Create team [POST /teams]

+ Request (application/json)

    + Body

            {
                "name": "team 1",
                "tactic_id": 1
            }


+ Response 201 (application/json)

    + Body

            {
                "id": 1,
                "name": "team 1",
                "tactic": "1 - 3 - 5 - 2",
                "players": [
                    {
                        "id": 1,
                        "name": "Lafont"
                    },
                    {
                        "id": 3,
                        "name": "Skhriniar"
                    },
                    {
                        "id": 5,
                        "name": "Felipe"
                    },
                    {
                        "id": 9,
                        "name": "De Sciglio"
                    },
                    {
                        "id": 11,
                        "name": "De Silvestri"
                    },
                    {
                        "id": 12,
                        "name": "Lazovic"
                    },
                    {
                        "id": 13,
                        "name": "Lulic"
                    },
                    {
                        "id": 16,
                        "name": "Vecino"
                    },
                    {
                        "id": 17,
                        "name": "Brozovic"
                    },
                    {
                        "id": 23,
                        "name": "Martinez"
                    },
                    {
                        "id": 25,
                        "name": "Zapata"
                    }
                ]
            }

### Change team tactic [PATCH /teams/:id/tactic/:tactic_id]

+ Request (application/json)

+ Response 200 (application/json)

    + Body

            {
                "id": 1,
                "name": "team 1",
                "tactic": "1 - 4 - 3 - 3",
                "players": [
                    {
                        "id": 2,
                        "name": "Strakosha"
                    },
                    {
                        "id": 6,
                        "name": "Cionek"
                    },
                    {
                        "id": 7,
                        "name": "Iacoponi"
                    },
                    {
                        "id": 8,
                        "name": "Milenkovic"
                    },
                    {
                        "id": 15,
                        "name": "Lucas Leiva"
                    },
                    {
                        "id": 21,
                        "name": "Iago Falque"
                    },
                    {
                        "id": 22,
                        "name": "Insigne"
                    },
                    {
                        "id": 3,
                        "name": "Skhriniar"
                    },
                    {
                        "id": 11,
                        "name": "De Silvestri"
                    },
                    {
                        "id": 16,
                        "name": "Vecino"
                    },
                    {
                        "id": 25,
                        "name": "Zapata"
                    }
                ]
            }
