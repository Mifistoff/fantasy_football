class Player < ApplicationRecord
  belongs_to :club
  belongs_to :team, optional: true

  has_and_belongs_to_many :positions

  validates :name, presence: true
  validates :name, uniqueness: true
end
