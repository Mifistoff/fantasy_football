class Team < ApplicationRecord
  belongs_to :tactic
  has_many :players

  validates :name, presence: true
  validates :name, uniqueness: true
end
