class Tactic < ApplicationRecord
  has_many :position_counts, dependent: :destroy
  has_many :teams

  validates :name, presence: true
  validates :name, uniqueness: true
end
