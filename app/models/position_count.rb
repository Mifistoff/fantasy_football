class PositionCount < ApplicationRecord
  belongs_to :position
  belongs_to :tactic

  validates :count, presence: true
end
