class PositionsController < ApplicationController
  # GET /positions
  def index
    render json: Position.all
  end

  # POST /positions
  def create
    position = Position.create!(create_positions_params)
    render json: position, status: 201
  end

  private

  def create_positions_params
    params.permit(%i[name])
  end
end
