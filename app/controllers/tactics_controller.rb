class TacticsController < ApplicationController
  # GET /tactics
  def index
    render json: Tactic.all
  end

  # POST /tactics
  def create
    tactic = Tactic.create(create_tactics_params)
    tactic.position_counts.create(create_positions_params[:position_counts])
    render json: tactic, status: 201
  end

  private

  def create_tactics_params
    params.permit(%i[name])
  end

  def create_positions_params
    params.permit(position_counts: [:position_id, :count])
  end
end
