class ClubsController < ApplicationController
  # GET /stats
  def stats
    club_stats = Club.all.order(:name).includes(:players).map do |x|
      { x.name => x.players.size }
    end
    render json: club_stats
  end
end
