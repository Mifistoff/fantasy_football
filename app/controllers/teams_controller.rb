class TeamsController < ApplicationController
  # GET /teams
  def index
    render json: Team.all
  end

  # POST /teams
  def create
    team = Team.create(create_team_params)
    team_players(team).update_all(team_id: team.id)
    render json: team, status: 201
  end

  # PATCH /teams/:id/tactic/:tactic_id
  def set_tactic
    team = Team.find(params[:id])
    team.players.update_all(team_id: nil)
    team.update(set_tactic_params)
    team_players(team).update_all(team_id: team.id)

    render json: team, status: 200
  end

  private

  def create_team_params
    params.permit(%i[name tactic_id])
  end

  def set_tactic_params
    params.permit(%i[tactic_id])
  end

  def team_players(team)
    players_positions = team.tactic.position_counts.includes(position: :players)
    players_id = players_positions.map do |pos|
      pos.position.players.where(team: nil).sample(pos.count)
    end.flatten.map(&:id)
    Player.where(id: players_id)
  end
end
