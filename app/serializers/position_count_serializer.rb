class PositionCountSerializer < ApplicationSerializer
  attributes %i[position_name count]

  def position_name
    object.position.name
  end
end
