class PositionSerializer < ApplicationSerializer
  attributes %i[id name]
end
