class PlayerSerializer < ApplicationSerializer
  attributes %i[id name]
end
