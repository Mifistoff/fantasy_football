class TacticSerializer < ApplicationSerializer
  attributes %i[id name]
  has_many :position_counts
end
