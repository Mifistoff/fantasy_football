class TeamSerializer < ApplicationSerializer
  attributes %i[id name tactic]

  has_many :players

  def tactic
    object.tactic.name
  end
end
