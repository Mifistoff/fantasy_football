Rails.application.routes.draw do
  resources :positions, only: %i[index create]
  resources :tactics, only: %i[index create]

  resources :teams, only: %i[index create]
  patch 'teams/:id/tactic/:tactic_id', to: 'teams#set_tactic'

  get 'stats', to: 'clubs#stats'
end
